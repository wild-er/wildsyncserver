import * as express from "express";
import * as socketio from "socket.io";
// @ts-ignore
import * as Tuio from "tuio-nw";
import { OneEuroFilter } from "./OneEuroFilter";
import * as chalk from "chalk";
import * as commandLineArgs from "command-line-args";
import { Server } from "node-osc";

const optionDefinitions = [
  { name: "ip", alias: "i", type: String },
  { name: "width", alias: "w", type: Number, defaultValue: 5 },
  { name: "height", alias: "h", type: Number, defaultValue: 2 },
  { name: "syncport", type: Number, defaultValue: 8080 },
  { name: "tuioinport", type: Number, defaultValue: 3333 },
  { name: "tuiooutport", type: Number, defaultValue: 8083 },
  { name: "oscinport", type: Number, defaultValue: -1 },
  { name: "tracelevel", type: Number, defaultValue: 1 },
];

/*------------------------------------------*/
/* Logging                                  */
/*------------------------------------------*/
enum LogType {
  PLAIN = 0,
  INFO = 1,
  WARNING = 2,
  ERROR = 3,
}
let TRACE_LEVEL = 0;

function log(msg: string, tl: number, ltype?: LogType) {
  if (tl <= TRACE_LEVEL) {
    if (typeof ltype !== "undefined") {
      switch (ltype) {
        case LogType.INFO: {
          console.log(`INFO: ${chalk.blue(msg)}`);
          break;
        }
        case LogType.WARNING: {
          console.log(`WARNING: ${chalk.hex("#FFA500")(msg)}`);
          break;
        }
        case LogType.ERROR: {
          console.log(`ERROR: ${chalk.red(msg)}`);
          break;
        }
        case LogType.PLAIN: {
          console.log(msg);
          break;
        }
      }
    } else {
      console.log(msg);
    }
  }
}

const options = commandLineArgs(optionDefinitions);
TRACE_LEVEL = options.tracelevel;
log("Command line " + process.argv, LogType.INFO);
log("ip " + options.ip, 1, LogType.INFO);
log("width " + options.width, 1, LogType.INFO);
log("height " + options.height, 1, LogType.INFO);
log("syncport " + options.syncport, 1, LogType.INFO);
log("tuioinport " + options.tuioinport, 1, LogType.INFO);
log("tuiooutport " + options.tuiooutport, 1, LogType.INFO);
log("oscinport " + options.oscinport, 1, LogType.INFO);

/*------------------------------------------*/
/* Synch                                    */
/*------------------------------------------*/

const appSync = express();
const viewSyncPort = options.syncport;
const viewSyncServer = appSync.listen(viewSyncPort, "0.0.0.0", () => {
  log(
    `Sync server listening on ${options.ip}:${viewSyncPort}`,
    1,
    LogType.INFO
  );
});
const viewSyncSocket = socketio.listen(viewSyncServer);
let masterSocket: socketio.Socket;

// whenever a client connects on port via
// a websocket, log that a client has connected
viewSyncSocket.on("connect", (socket: socketio.Socket) => {
  socket.on("ID", (message) => {
		if (message.indexOf("MASTER") != -1) {
			console.log(message + " listening for updates");
			if (message==="MASTER")
				masterSocket = socket;
      socket.on("Rpc", (message) => {
        // directly forward viewport transform commands to all clients
        // console.log("Rpc -> "+JSON.stringify(message));
        //Forward message to slaves
        socket.broadcast.emit("Rpc", message);
      });
    } else {
      console.log(`Slave ${message} listening for updates`);
    }
  });
});

/*------------------------------------------*/
/* TUIO                                    */
/*------------------------------------------*/

const EPSILON = 0.001;

interface filterData {
  filter: OneEuroFilter;
  lastTimeStamp: number;
  lastValue: number;
}

const filtersX: Array<filterData> = [];
const filtersY: Array<filterData> = [];

for (let i = 0; i < 32; i++) {
  const fx: OneEuroFilter = new OneEuroFilter(60, 1, 1, 1);
  const dataX: filterData = { filter: fx, lastTimeStamp: 0, lastValue: 0 };
  filtersX.push(dataX);
  const fy: OneEuroFilter = new OneEuroFilter(60, 1, 1, 1);
  const dataY: filterData = { filter: fy, lastTimeStamp: 0, lastValue: 0 };
  filtersY.push(dataY);
}

interface tuioMessage {
  xPos: number;
  yPos: number;
  cursorId: number;
}

function filter(msg: tuioMessage) {
  if (
    !isNaN(msg.yPos) &&
    !isNaN(msg.xPos) &&
    msg.cursorId >= 0 &&
    msg.cursorId < 32
  ) {
    const timestamp = new Date().getTime() * 0.001;
    if (Math.abs(timestamp - filtersX[msg.cursorId].lastTimeStamp) < EPSILON)
      msg.xPos = filtersX[msg.cursorId].lastValue;
    else {
      msg.xPos = filtersX[msg.cursorId].filter.filter(msg.xPos, timestamp);
      filtersX[msg.cursorId].lastValue = msg.xPos;
    }
    filtersX[msg.cursorId].lastTimeStamp = timestamp;
    if (Math.abs(timestamp - filtersY[msg.cursorId].lastTimeStamp) < EPSILON)
      msg.yPos = filtersY[msg.cursorId].lastValue;
    else {
      msg.yPos = filtersY[msg.cursorId].filter.filter(msg.yPos, timestamp);
      filtersY[msg.cursorId].lastValue = msg.yPos;
    }
    filtersY[msg.cursorId].lastTimeStamp = timestamp;
  }
}

function resetFilter(id: number) {
  filtersX[id].filter.reset();
}

const appTuio = express();
appTuio.set("port", options.tuiooutport);

const http = require("http").Server(appTuio);
const ioTuio = socketio(http);

ioTuio.on("connection", () => {
  log("TUIO client connected", 1, LogType.INFO);
});

//Listen and forward TUIO cursor events
const tuioClient = new Tuio.Client({
  host: options.ip,
  port: options.tuioinport,
});
function onAddTuioCursor(addCursor: tuioMessage) {
  resetFilter(addCursor.cursorId);
  filter(addCursor);
  ioTuio.emit("onAdd", addCursor);
  console.log(addCursor);
}
function onUpdateTuioCursor(updateCursor: tuioMessage) {
  filter(updateCursor);
  ioTuio.emit("onUpdate", updateCursor);
}
function onRemoveTuioCursor(removeCursor: tuioMessage) {
  filter(removeCursor);
  ioTuio.emit("onRemove", removeCursor);
}
tuioClient.on("addTuioCursor", onAddTuioCursor);
tuioClient.on("updateTuioCursor", onUpdateTuioCursor);
tuioClient.on("removeTuioCursor", onRemoveTuioCursor);
tuioClient.listen();

http.listen(options.tuiooutport, () => {
  log(
    `Tuio server listening on ${options.ip}:${options.tuiooutport}`,
    1,
    LogType.INFO
  );
});

/*------------------------------------------*/
/* OSC                                      */
/*------------------------------------------*/
if (options.oscinport != -1) {
  //Start osc listener
  const oscServer = new Server(options.oscinport, "0.0.0.0", () => {
    console.log("OSC Server is listening");
    log(
      `OSC Server is listening on port ${options.oscinport}`,
      1,
      LogType.INFO
    );
  });

  oscServer.on("message", function (msg) {
    log(`OSC Message: ${msg}`, 1, LogType.INFO);
    if (masterSocket) masterSocket.emit("Osc", msg);
  });
}
